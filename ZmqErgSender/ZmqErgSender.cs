﻿
namespace ZmqErgSender
{
    using System.Collections.Generic;
    using NetMQ;
    using NetMQ.Sockets;
    using ProtoBuf;
    using FitzLanePlugin.Interfaces;
    using FitzLanePlugin;

    class ZmqErgSender : IErgSender
    {
        public bool IsConnected { get; private set; }

        public string SenderName => "ZmqErgSender";
        public string SenderDescription => "Sends all the bots to the Fitznet";
        public string SenderVersion => "1.0.0";
        public SenderConfigViewModel Config => new SenderConfigViewModel();

        PublisherSocket pubSocket = null;

        public ZmqErgSender()
        {
            IsConnected = false;
        }

        ~ZmqErgSender()
        {
            pubSocket.Close();
        }

        public void Connect(string givenAddress)
        {
            if (IsConnected)
            {
                return;
            }

            pubSocket = new PublisherSocket();
            pubSocket.Bind(givenAddress);

            IsConnected = true;
        }

        public void SendErgs(IList<IPlayer> playerList)
        {
            if (!IsConnected)
            {
                return;
            }

            var message = new NetMQMessage();
            message.Append("EasyErgsocket");

            foreach (IPlayer player in playerList)
            {
                using (var buffer = new System.IO.MemoryStream())
                {
                    Serializer.Serialize(buffer, player.PlayerErg);
                    message.Append(buffer.ToArray());
                }
            }

            pubSocket.TrySendMultipartMessage(message);
        }
    }
}
