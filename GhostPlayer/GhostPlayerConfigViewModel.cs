﻿
namespace GhostPlayer
{
    using FitzLanePlugin;
    using Microsoft.Win32;
    using System;
    using System.IO;
    using System.Runtime.Serialization.Json;
    using System.Text;
    using System.Text.RegularExpressions;

    public class GhostPlayerConfigViewModel : PlayerConfigViewModel
    {
        private string filename = "";
        public string RecordName { get; set; } = "";

        public override string GetPlayerConfig()
        {
            GhostPlayerConfig botCfg = new GhostPlayerConfig
            {
                Filename = filename
            };
            MemoryStream memStream = new MemoryStream();
            DataContractJsonSerializer botSerializer = new DataContractJsonSerializer(typeof(GhostPlayerConfig));
            botSerializer.WriteObject(memStream, botCfg);
            return Encoding.UTF8.GetString(memStream.ToArray());
        }

        public override void SetPlayerConfig(string config)
        {
            //read the config
            MemoryStream memStream = new MemoryStream();
            StreamWriter strWriter = new StreamWriter(memStream);
            strWriter.Write(config);
            strWriter.Flush();
            memStream.Position = 0;
            DataContractJsonSerializer playerSerializer = new DataContractJsonSerializer(typeof(GhostPlayerConfig));
            GhostPlayerConfig botCfg = (GhostPlayerConfig)playerSerializer.ReadObject(memStream);
            filename = botCfg.Filename;
            UpdateRecordName(filename);
        }

        public void ChooseRecording()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Recordings (*.db)|*.db|All files (*.*)|*.*";
            var recordpath = AppDomain.CurrentDomain.BaseDirectory + "records";
            openFileDialog.InitialDirectory = recordpath;
            if (openFileDialog.ShowDialog() == true)
            {
                filename = openFileDialog.FileName;
                UpdateRecordName(filename);
            }
        }

        private void UpdateRecordName(string filename)
        {
            //get a better readable name out of the full path...
            //Regex...
            //Input: .\\session_Firstname Surname_17-10-18_18-26-14.db
            //Regex: session_(.*)_([0-9-]+_[0-9-]+).db
            //Group1: Firstname Surname
            //Group2: 17-10-18_18-26-14
            var pattern = "session_(.*)_([0-9-]+_[0-9-]+).db";
            var match = Regex.Match(filename, pattern);
            if (match.Groups.Count >= 3)
            {
                var recordDate = DateTime.ParseExact(match.Groups[2].Value, "y-M-d_H-m-s", null);

                RecordName = match.Groups[1].Value;
                RecordName += ": " + recordDate.ToString("d. MMM. yyyy, HH:mm");
            }
            NotifyOfPropertyChange(() => RecordName);
        }
    }
}
