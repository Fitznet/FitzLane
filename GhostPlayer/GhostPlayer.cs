﻿using EasyErgsocket;
using FitzLanePlugin.Interfaces;
using SQLite;
using System;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text.RegularExpressions;

namespace GhostPlayer
{
    class GhostPlayer : IPlayer
    {
        private SQLiteConnection db = null;
        private readonly double timestampDelta = 0.01; //how often should we update?
        private double lastTimestamp = -1.0; //start at a low value so the first update goes through in every case

        //this is needed to measure the time if this bot is not attached to any parent lane
        private DateTime starttime = DateTime.MinValue;

        public string ParentName { get; set; }
        public bool IsRecordingActive { get; set; } = false;
        public Erg PlayerErg { get; set; }

        public double TotalDistance { get; set; }
        public double TotalExerciseTime { get; set; }

        public GhostPlayer(string name, string config)
        {
            //init an empty erg
            PlayerErg = new Erg
            {
                cadence = 0,
                calories = 1,
                distance = 0.0,
                ergtype = ErgType.ROW,
                exerciseTime = 0.0,
                heartrate = 0,
                name = name,
                paceInSecs = 0,
                playertype = PlayerType.BOT,
                power = 0,
                ergId = name
            };

            //read the config
            MemoryStream memStream = new MemoryStream();
            StreamWriter strWriter = new StreamWriter(memStream);
            strWriter.Write(config);
            strWriter.Flush();
            memStream.Position = 0;
            DataContractJsonSerializer playerSerializer = new DataContractJsonSerializer(typeof(GhostPlayerConfig));
            GhostPlayerConfig cfg = (GhostPlayerConfig)playerSerializer.ReadObject(memStream);

            //read the db and fill in what we already know
            db = new SQLiteConnection(cfg.Filename);
            TotalDistance = GetTotalDistance(db);
            TotalExerciseTime = GetTotalExerciseTime(db);
            InitPlayerName(cfg.Filename);

            //first update, to get even the last things inited
            Update(0.0);
        }
        
        public void Reset()
        {
            starttime = DateTime.MinValue;
        }

        public void Update(Erg givenParent = null)
        {
            if (starttime == DateTime.MinValue)
            {
                starttime = DateTime.Now;
            }
            if (givenParent == null)
            {
                //if there is no parent to give the time use the time that has passed since starting the application
                TimeSpan timeSinceStart = DateTime.Now - starttime;
                PlayerErg.exerciseTime = timeSinceStart.TotalSeconds;
            }
            else
            {
                PlayerErg.exerciseTime = givenParent.exerciseTime;
            }
            Update(PlayerErg.exerciseTime);
        }
        private void Update(double timestamp)
        {
            //do not update if nothing changed (or simply not enough has changed)...
            if (Math.Abs(lastTimestamp - timestamp) < timestampDelta)
            {
                return;
            }
            lastTimestamp = timestamp;

            //When the given timestamp exceeds our timestamp we should start again from the beginning. This way all bots will continue to row, even when the recording is already over
            int numCompleted = (int)(timestamp / TotalExerciseTime);
            if (timestamp > TotalExerciseTime)
            {
                timestamp = (timestamp % TotalExerciseTime);
            }

            //update the values to reflect what's been stored at the given timestamp
            var results = db.Query<rowdata>("SELECT * FROM rowdata WHERE timestamp >= ? LIMIT 1;", (timestamp));
            if (results.Count > 0)
            {
                PlayerErg.distance = results[0].distance + (numCompleted * TotalDistance);
                PlayerErg.exerciseTime = results[0].timestamp;
                PlayerErg.cadence = results[0].spm;
                PlayerErg.paceInSecs = (uint)results[0].pace;
                PlayerErg.calories = results[0].calories;
                PlayerErg.power = results[0].power;
                PlayerErg.heartrate = results[0].heartrate;
            }
        }

        private void InitPlayerName(string filename)
        {
            //Input: .\\session_Firstname Surname_17-10-18_18-26-14.db
            //Regex: session_(.*)_([0-9-]+_[0-9-]+).db
            //Group1: Firstname Surname
            //Group2: 17-10-18_18-26-14

            var pattern = "session_(.*)_([0-9-]+_[0-9-]+).db";
            var match = Regex.Match(filename, pattern);
            if (match.Groups.Count >= 3)
            {
                PlayerErg.name = match.Groups[1].Value;
                PlayerErg.ergId = PlayerErg.name;
                //WorkoutDate = DateTime.ParseExact(match.Groups[2].Value, "y-M-d_H-m-s", null);
            }
        }

        private double GetTotalExerciseTime(SQLiteConnection db)
        {
            return GetMaxDouble("timestamp", db);
        }

        private double GetTotalDistance(SQLiteConnection db)
        {
            return GetMaxDouble("distance", db);
        }

        private double GetDistanceAt(SQLiteConnection db, double timestamp)
        {
            return GetDoubleAt("distance", db, timestamp);
        }

        private double GetDoubleAt(string fieldName, SQLiteConnection db, double timestamp)
        {
            string selectDoubleAt = "select MAX(" + fieldName + ") from rowdata WHERE timestamp <= " + timestamp;
            SQLiteCommand selectDoubleCmd = new SQLiteCommand(db)
            {
                CommandText = selectDoubleAt
            };

            try
            {
                return selectDoubleCmd.ExecuteScalar<double>();
            }
            catch (NullReferenceException)
            {
                //No MAX in DB, return 0.0 then (good enough for our case)
                return 0.0;
            }
        }

        private double GetMaxDouble(string fieldName, SQLiteConnection db)
        {
            string selectMaxDouble = "select MAX(" + fieldName + ") from rowdata";
            SQLiteCommand selectMaxCmd = new SQLiteCommand(db)
            {
                CommandText = selectMaxDouble
            };

            try
            {
                return selectMaxCmd.ExecuteScalar<double>();
            }
            catch (NullReferenceException)
            {
                //No MAX in DB, return 0.0 then (good enough for our case)
                return 0.0;
            }
        }
    }
}
