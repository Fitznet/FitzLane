﻿using System.Runtime.Serialization;

namespace GhostPlayer
{
    [DataContract]
    class GhostPlayerConfig
    {
        [DataMember]
        public string Filename="";
    }
}
