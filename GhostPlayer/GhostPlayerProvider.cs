﻿
using FitzLanePlugin.Interfaces;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
using FitzLanePlugin;

namespace GhostPlayer
{
    class GhostPlayerProvider : IPlayerProvider
    {
        public IPlayer GetPlayer(string name, string config)
        {
            return new GhostPlayer(name, config);
        }

        public string GetDefaultPlayerConfig()
        {
            GhostPlayerConfig botCfg = new GhostPlayerConfig();
            MemoryStream memStream = new MemoryStream();
            DataContractJsonSerializer botSerializer = new DataContractJsonSerializer(typeof(GhostPlayerConfig));
            botSerializer.WriteObject(memStream, botCfg);
            return Encoding.UTF8.GetString(memStream.ToArray());
        }

        public bool IsValidPlayertype(string playerType)
        {
            if (playerType == "GhostPlayer")
            {
                return true;
            }
            return false;
        }

        public PlayerConfigViewModel GetPlayerConfigViewModel()
        {
            return new GhostPlayerConfigViewModel();
        }

        public string PlayerDescription
        {
            get { return "Replays any given recording. Have fun racing the past!"; }
        }
        public string PlayerName
        {
            get { return "GhostPlayer"; }
        }
        public string PlayerVersion
        {
            get { return "1.0.0"; }
        }
    }
}
