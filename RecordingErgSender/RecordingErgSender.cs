﻿using FitzLanePlugin;
using FitzLanePlugin.Interfaces;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecordingErgSender
{
    public class RecordingErgSender : IErgSender
    {
        public string SenderName => "RecordingSender";
        public string SenderDescription => "Records all marked ergs into a file so you can share and replay them later on";
        public string SenderVersion => "1.0.0";
        public SenderConfigViewModel Config => new SenderConfigViewModel();

        private Dictionary<string, SQLiteConnection> databases = new Dictionary<string, SQLiteConnection>();
        private readonly Dictionary<string, uint> insertCounter = new Dictionary<string, uint>();
        private readonly Dictionary<string, double> lastCommittedDistance = new Dictionary<string, double>();
        private readonly double distanceBeforeCommitting = 1.0;
        private readonly uint numInsertsBeforeCommitIsTriggered = 50;
        private bool isAlreadyCommitting = false;

        private DateTime timestamp = DateTime.MinValue;

        ~RecordingErgSender()
        {
            foreach (var db in databases.Values)
            {
                db.Close();
                db.Dispose();
            }
        }

        public void SendErgs(IList<IPlayer> playerList)
        {
            if(timestamp == DateTime.MinValue)
            {
                timestamp = DateTime.Now;
            }

            var recordPlayers = playerList.Where(p => p.IsRecordingActive);
            foreach (var player in recordPlayers)
            {
                //check if there already is a file for the given player
                StringBuilder idBuilder = new StringBuilder();
                idBuilder.Append("records/");
                idBuilder.Append("session_");
                idBuilder.Append(player.PlayerErg.name);
                idBuilder.Append("_");
                idBuilder.Append(timestamp.ToString("%y-%M-%d_%H-%m-%s"));
                idBuilder.Append(".db");
                var playerId = idBuilder.ToString();

                //create a new db if it hasn't been created yet
                if (!databases.ContainsKey(playerId))
                {
                    var newDb = new SQLiteConnection(playerId);
                    newDb.CreateTable<rowdata>();
                    databases[playerId] = newDb;
                    insertCounter[playerId] = 0;
                    lastCommittedDistance[playerId] = 0.0;
                }

                //add the current record to the recordings
                var db = databases[playerId];
                if (!db.IsInTransaction)
                {
                    db.BeginTransaction();
                }

                var erg = player.PlayerErg;
                if((lastCommittedDistance[playerId] + distanceBeforeCommitting) < erg.distance) //do not insert when the boat is not moving much... keeps the number of inserts low
                {
                    db.Insert(new rowdata()
                    {
                        avgpace = 0.0,
                        calhr = 0.0,
                        calories = erg.calories,
                        distance = erg.distance,
                        heartrate = erg.heartrate,
                        pace = erg.paceInSecs,
                        power = erg.power,
                        spm = erg.cadence,
                        timestamp = erg.exerciseTime
                    });
                    lastCommittedDistance[playerId] = erg.distance;
                    insertCounter[playerId]++;

                    if (!isAlreadyCommitting &&
                        insertCounter[playerId] > numInsertsBeforeCommitIsTriggered)
                    {
                        isAlreadyCommitting = true;
                        Task.Run(() =>
                        {
                            db.Commit();
                            insertCounter[playerId] = 0;
                            isAlreadyCommitting = false;
                        });
                    }
                }
            }
        }

        public void Connect(string address) { } //this is not needed for this type of "sender"
    }
}
