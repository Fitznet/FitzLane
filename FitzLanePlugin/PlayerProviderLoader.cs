﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Reflection;
using FitzLanePlugin.Interfaces;

namespace FitzLanePlugin
{
    public class PlayerProviderLoader
    {
        List<IPlayerProvider> loadedProviders = new List<IPlayerProvider>();
        
        public void LoadPlayerProviders(string pluginPath)
        {
            IEnumerable<Assembly> providerAssemblies = GetPlayerProviderAssemblies(pluginPath);
            foreach (Assembly assembly in providerAssemblies)
            {
                try
                {
                    loadedProviders.Add(LoadPlayerProvider(assembly));
                }
                catch (ArgumentException)
                {
                    //cannot load the given file...
                    continue;
                }
            }
        }

        public IList<Assembly> GetPlayerProviderAssemblies(string pluginPath)
        {
            if (!Directory.Exists(pluginPath))
            {
                //the plugin directory does not exist
                throw new ArgumentOutOfRangeException("Cannot find assemblies at " + pluginPath + ": Path does not exist!");
            }

            IList<Assembly> assemblyList = new List<Assembly>();
            DirectoryInfo d = new DirectoryInfo(pluginPath);
            foreach (var file in d.GetFiles("*.dll"))
            {
                try
                {
                    assemblyList.Add(LoadPlayerProviderAssembly(file.FullName));
                }
                catch (ArgumentException)
                {
                    //cannot load the given file...
                    continue;
                }
            }

            return assemblyList;
        }

        public List<string> GetPlayerNames()
        {
            List<string> names = new List<string>();
            foreach (IPlayerProvider provider in loadedProviders)
            {
                names.Add(provider.PlayerName);
            }
            return names;
        }

        public List<IPlayerProvider> GetPlayerProvider()
        {
            return loadedProviders;
        }


        private Assembly LoadPlayerProviderAssembly(string file)
        {
            try
            {
                return Assembly.LoadFile(file);
            }
            catch (Exception)
            {
                //unable to load...
                throw new ArgumentException("Unable to load IPlayerProvider assembly from " + file);
            }

            //unable to load...
            throw new ArgumentException("Unable to load IPlayerProvider assembly from " + file);
        }

        private IPlayerProvider LoadPlayerProvider(Assembly assembly)
        {
            Type pluginInfo = null;
            try
            {
                Type[] types = assembly.GetTypes();
                Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
                Assembly core = AppDomain.CurrentDomain.GetAssemblies().Single(x => x.GetName().Name.Equals("FitzLanePlugin"));
                Type type = core.GetType("FitzLanePlugin.Interfaces.IPlayerProvider");
                foreach (var t in types)
                {
                    if (type.IsAssignableFrom((Type)t))
                    {
                        pluginInfo = t;
                        break;
                    }
                }

                if (pluginInfo != null)
                {
                    object o = Activator.CreateInstance(pluginInfo);
                    IPlayerProvider playerProvider = (IPlayerProvider)o;
                    //returning from somewhere within the method... this needs to get refactored...
                    return playerProvider;
                }
            }
            catch (Exception ex)
            {
                //unable to load...
                throw new ArgumentException("Unable to load IPlayerProvider from " + assembly.FullName + "(" + ex.ToString() + ")");
            }
            
            //unable to load...
            throw new ArgumentException("Unable to load IPlayerProvider from " + assembly.FullName);
        }
    }
}
