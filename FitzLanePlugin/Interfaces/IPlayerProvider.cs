﻿
namespace FitzLanePlugin.Interfaces
{
    public interface IPlayerProvider
    {
        string PlayerName { get; }
        string PlayerDescription { get; }
        string PlayerVersion { get; }

        bool IsValidPlayertype(string playerType);
        IPlayer GetPlayer(string name, string config);
        string GetDefaultPlayerConfig();

        PlayerConfigViewModel GetPlayerConfigViewModel();
    }
}
