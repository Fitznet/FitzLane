﻿
using EasyErgsocket;

namespace FitzLanePlugin.Interfaces
{
    public interface IPlayer
    {
        string ParentName { get; set; }
        bool IsRecordingActive { get; set; }
        Erg PlayerErg { get; }

        void Update(Erg givenParent = null);
        void Reset();
    }
}
