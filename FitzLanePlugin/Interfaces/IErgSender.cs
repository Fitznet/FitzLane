﻿using System.Collections.Generic;

namespace FitzLanePlugin.Interfaces
{
    public interface IErgSender
    {
        string SenderName { get; }
        string SenderDescription { get; }
        string SenderVersion { get; }
        SenderConfigViewModel Config { get; }

        void Connect(string address);
        void SendErgs(IList<IPlayer> playerList);
    }
}
