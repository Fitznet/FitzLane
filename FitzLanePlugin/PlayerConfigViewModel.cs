﻿
using Caliburn.Micro;

namespace FitzLanePlugin
{
    public class PlayerConfigViewModel : Screen
    {
        public virtual string GetPlayerConfig()
        {
            return "";
        }

        public virtual void SetPlayerConfig(string config)
        {}
    }
}
