﻿

namespace FitzLaneUI
{
    using Caliburn.Micro;
    using FitzLaneConfig;
    using FitzLanePlugin;
    using FitzLanePlugin.Interfaces;
    using System.Windows.Media;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Events;
    class LaneContainerViewModel : IHandle<RemovePlayerEvent>, IHandle<NewPlayerEvent>, IHandle<EditPlayerEvent>
    {
        private readonly IEventAggregator eventAggregator;

        //First: ErgId, Second: Player instance... putting the players into a map makes it easier to find them later on
        IDictionary<string, IPlayer> players = new Dictionary<string, IPlayer>();
        readonly IList<IErgSender> ergSenders = new List<IErgSender>();

        const string configFilePath = "worldConfig.json";
        ConfigReader reader = null;

        public BindableCollection<SingleLaneContainerViewModel> Lanes { get; } = new BindableCollection<SingleLaneContainerViewModel>();
        public PlayerProviderLoader MyPlayerProviderLoader { get; set; }
        public ErgSenderLoader MyErgSenderLoader { get; set; }

        public LaneContainerViewModel(IEventAggregator eventAggregator, PlayerProviderLoader playerProviderLoader, ErgSenderLoader ergSenderLoader)
        {
            this.eventAggregator = eventAggregator;
            eventAggregator.Subscribe(this);
            
            //fill in empty lanes in the beginning
            ClearLanes();

            MyPlayerProviderLoader = playerProviderLoader;
            MyErgSenderLoader = ergSenderLoader;

            //Load config, get players
            reader = new ConfigReader(configFilePath);
            foreach (Lane lane in reader.getLanes())
            {
                IPlayer player = CreatePlayerFromLane(lane);
                LaneViewModel newPlayerLane = new FullLaneViewModel(eventAggregator, player, lane);
                Lanes[lane.laneIndex] = new SingleLaneContainerViewModel(lane.laneIndex, newPlayerLane);
            }

            //init the senders
            ergSenders = MyErgSenderLoader.GetErgSender();
            foreach (IErgSender sender in ergSenders)
            {
                //TODO: This inits every sender with a zmq address... better implement some kind of config mechanism...
                sender.Connect("tcp://127.0.0.1:21744");
            }
        }

        public void ClearLanes()
        {
            Lanes.Clear();

            EmptyLaneViewModel empty0 = new EmptyLaneViewModel(eventAggregator, MyPlayerProviderLoader, 0);
            EmptyLaneViewModel empty1 = new EmptyLaneViewModel(eventAggregator, MyPlayerProviderLoader, 1);
            EmptyLaneViewModel empty2 = new EmptyLaneViewModel(eventAggregator, MyPlayerProviderLoader, 2);
            EmptyLaneViewModel empty3 = new EmptyLaneViewModel(eventAggregator, MyPlayerProviderLoader, 3);
            EmptyLaneViewModel empty4 = new EmptyLaneViewModel(eventAggregator, MyPlayerProviderLoader, 4);
            Lanes.Add(new SingleLaneContainerViewModel(0, empty0));
            Lanes.Add(new SingleLaneContainerViewModel(1, empty1));
            Lanes.Add(new SingleLaneContainerViewModel(2, empty2));
            Lanes.Add(new SingleLaneContainerViewModel(3, empty3));
            Lanes.Add(new SingleLaneContainerViewModel(4, empty4));
        }

        public void WriteCurrentConfig(string configfile)
        {
            //Write the config
            //get a list of all lanes, write them to file
            List<Lane> plainLanes = new List<Lane>();
            foreach (SingleLaneContainerViewModel singleLane in Lanes)
            {
                //get the plain lanes (needed to write them to the config)
                if (singleLane.CurrentLane is FullLaneViewModel fullLane)
                {
                    plainLanes.Add(fullLane.Lane);
                }
            }
            ConfigWriter.WriteConfig(configFilePath, plainLanes);
        }

        public void WriteCurrentConfig()
        {
            WriteCurrentConfig(configFilePath);
        }

        public void StartSimulation()
        {
            WriteCurrentConfig();

            //notify all the lanes that we're started
            foreach (SingleLaneContainerViewModel singleLane in Lanes)
            {
                //start/stop the lanes (they disable control when started etc)
                singleLane.CurrentLane.IsStarted = true;
            }

            //subscribe to the WPF Rendering event to get some sort of GameLoop
            CompositionTarget.Rendering += UpdateLanes;
        }

        public void StopSimulation()
        {
            //subscribe to the WPF Rendering event to get some sort of GameLoop
            CompositionTarget.Rendering -= UpdateLanes;

            //start/stop the lanes (they disable control when started etc)
            foreach (SingleLaneContainerViewModel singleLane in Lanes)
            {
                singleLane.CurrentLane.IsStarted = false;
            }

            foreach (IPlayer player in players.Values)
            {
                player.Reset();
            }
        }

        private void UpdateLanes(object sender, EventArgs e)
        {
            //Update all the players
            //This loop tries to update at least 1 player in each iteration, if it fails the loop is either over or there is a failed parent/child link
            List<string> alreadyUpdatedPlayers = new List<string>();
            while (alreadyUpdatedPlayers.Count < players.Count)
            {
                //see if there's a player that we could update
                bool hasPlayerBeenUpdated = false;
                foreach (IPlayer player in players.Values)
                {
                    //do not update the same player twice
                    if (alreadyUpdatedPlayers.Contains(player.PlayerErg.name))
                    {
                        continue;
                    }

                    //update all the player without parents or whose parents have already been updated
                    if (player.ParentName == "")
                    {
                        player.Update(null);
                        alreadyUpdatedPlayers.Add(player.PlayerErg.name);
                        hasPlayerBeenUpdated = true;
                    }
                    else if (alreadyUpdatedPlayers.Contains(player.ParentName))
                    {
                        EasyErgsocket.Erg parent = players[player.ParentName].PlayerErg;
                        player.Update(parent);
                        alreadyUpdatedPlayers.Add(player.PlayerErg.name);
                        hasPlayerBeenUpdated = true;
                    }
                    else
                    {
                        //This lane has a parent that does not exist yet...
                        continue;
                    }
                }

                if (!hasPlayerBeenUpdated)
                {
                    //no player updated... there seems to be a problem?
                    //TODO: What should we do? This typically means there is a erg without parent or a circular dependency...
                    break;
                }
            }
            
            //update the LaneViews
            foreach (SingleLaneContainerViewModel singleLane in Lanes)
            {
                if (singleLane.CurrentLane is FullLaneViewModel fullLane)
                {
                    fullLane.Player = players[fullLane.Player.PlayerErg.name];
                }
            }

            //send the bots
            IList<IPlayer> playerList = players.Values.ToList();
            foreach (IErgSender ergSender in ergSenders)
            {
                ergSender.SendErgs(playerList);
            }
        }

        IPlayer CreatePlayerFromLane(Lane givenLane)
        {
            //if the name is already used add a postfix to it
            var originalName = givenLane.name;
            var counter = 1;
            while(players.ContainsKey(givenLane.name))
            {
                givenLane.name = originalName + " (" + counter + ")";
                ++counter;
            }

            //create the actual player
            foreach (IPlayerProvider provider in MyPlayerProviderLoader.GetPlayerProvider())
            {
                if (provider.IsValidPlayertype(givenLane.playerType))
                {
                    IPlayer player = provider.GetPlayer(givenLane.name, givenLane.playerConfig);
                    player.ParentName = givenLane.parentName;
                    players[player.PlayerErg.name] = player;
                    return player;
                }
            }

            throw new Exception("Plugin for " + givenLane.playerType + " not found!");
        }

        public void Handle(RemovePlayerEvent playerEvent)
        {
            //we need to get the index of the given player because we need to add an empty lane after removing the player
            int playerIndex = -1;

            //remove the LaneViewModel and every other reference to that player
            foreach (SingleLaneContainerViewModel singleLane in Lanes)
            {
                if (singleLane.CurrentLane is FullLaneViewModel fullLane)
                {
                    if (fullLane.Player.PlayerErg.name == playerEvent.Player.PlayerErg.name)
                    {
                        playerIndex = Lanes.IndexOf(singleLane);
                        Lanes.Remove(singleLane);
                        break;
                    }
                }
            }

            players.Remove(playerEvent.Player.PlayerErg.name);
            
            //Fill in an empty player again
            if(playerIndex >= 0)
            {
                LaneViewModel emptyModel = new EmptyLaneViewModel(eventAggregator, MyPlayerProviderLoader, playerIndex);
                Lanes.Insert(playerIndex, new SingleLaneContainerViewModel(playerIndex, emptyModel));
            }
        }

        public void Handle(NewPlayerEvent message)
        {
            ConfigurePlayerViewModel newPlayerDialog = new ConfigurePlayerViewModel(MyPlayerProviderLoader, players.Values, message.LaneIndex);
            ConfigurePlayer(newPlayerDialog);

        }

        public void Handle(EditPlayerEvent message)
        {
            ConfigurePlayerViewModel editPlayerDialog = new ConfigurePlayerViewModel(MyPlayerProviderLoader, players.Values, message.EditLane);
            //Remove player that is to be edited... we'll create a new one out of it during ConfigurePlayer
            if(players.ContainsKey(message.EditLane.name))
            {
                players.Remove(message.EditLane.name);
            }
            ConfigurePlayer(editPlayerDialog);
        }

        public void ConfigurePlayer(ConfigurePlayerViewModel playerDialog)
        {
            IWindowManager windowManager = new WindowManager();
            windowManager.ShowDialog(playerDialog);

            IPlayer player = CreatePlayerFromLane(playerDialog.ConfiguredLane);
            LaneViewModel newPlayerLane = new FullLaneViewModel(eventAggregator, player, playerDialog.ConfiguredLane);
            Lanes[playerDialog.ConfiguredLane.laneIndex] = new SingleLaneContainerViewModel(playerDialog.ConfiguredLane.laneIndex, newPlayerLane);
        }
    }
}
