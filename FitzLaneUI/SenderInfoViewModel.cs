﻿
namespace FitzLaneUI
{
    using FitzLanePlugin.Interfaces;

    public class SenderInfoViewModel
    {
        public SenderInfoViewModel(IErgSender sender)
        {
            SenderName = sender.SenderName;
            SenderDescription = sender.SenderDescription;
            SenderVersion = "Version: " + sender.SenderVersion;
        }

        public string SenderName
        {
            get; set;
        }
        public string SenderDescription
        {
            get; set;
        }
        public string SenderVersion
        {
            get; set;
        }
    }
}
