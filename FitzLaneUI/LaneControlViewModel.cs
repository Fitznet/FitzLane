﻿
namespace FitzLaneUI
{
    using Caliburn.Micro;
    using Events;

    public class LaneControlViewModel : PropertyChangedBase
    {
        private readonly IEventAggregator eventAggregator;
        private bool isStarted = false;
        private string startStopText = "Start Exercise";


        public LaneControlViewModel(IEventAggregator eventAggregator)
        {
            this.eventAggregator = eventAggregator;
        }

        public string StartStopText
        {
            get
            {
                return startStopText;
            }
            set
            {
                startStopText = value;
                NotifyOfPropertyChange(() => StartStopText);
            }
        }

        public void StartStop()
        {
            if(isStarted)
            {
                eventAggregator.PublishOnUIThread(new StopEvent());
                StartStopText = "Start Exercise";
                isStarted = !isStarted;
            }
            else
            {
                eventAggregator.PublishOnUIThread(new StartEvent());
                StartStopText = "Stop Exercise";
                isStarted = !isStarted;
            }
        }
    }
}
