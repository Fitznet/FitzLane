﻿
namespace FitzLaneUI
{
    public class SingleLaneContainerViewModel
    {
        public SingleLaneContainerViewModel(int givenIndex, LaneViewModel givenLaneViewModel)
        {
            CurrentLane = givenLaneViewModel;
            LaneIndexText = "Lane #" + givenIndex;
        }

        public string LaneIndexText
        {
            get;
            set;
        }
        public LaneViewModel CurrentLane
        {
            get;
            set;
        }
    }
}
