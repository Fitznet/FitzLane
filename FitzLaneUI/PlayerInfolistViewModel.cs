﻿
namespace FitzLaneUI
{
    using Caliburn.Micro;
    using FitzLanePlugin;
    using FitzLanePlugin.Interfaces;
    using System.Collections.Generic;

    class PlayerInfolistViewModel
    {
        BindableCollection<PlayerInfoViewModel> players = new BindableCollection<PlayerInfoViewModel>();

        public PlayerInfolistViewModel(PlayerProviderLoader playerProviderLoader)
        {
            List<IPlayerProvider> fitzProviders = playerProviderLoader.GetPlayerProvider();
            foreach (IPlayerProvider provider in fitzProviders)
            {
                PlayerInfoViewModel newInfo = new PlayerInfoViewModel(provider);
                Players.Add(newInfo);
            }
            
        }

        public BindableCollection<PlayerInfoViewModel> Players
        {
            get
            {
                return players;
            }
        }
    }
}
