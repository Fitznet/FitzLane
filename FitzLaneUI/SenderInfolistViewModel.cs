﻿

namespace FitzLaneUI
{
    using Caliburn.Micro;
    using FitzLanePlugin;
    using FitzLanePlugin.Interfaces;
    using System.Collections.Generic;

    class SenderInfolistViewModel : PropertyChangedBase
    {
        BindableCollection<SenderInfoViewModel> senders = new BindableCollection<SenderInfoViewModel>();
        ErgSenderLoader senderLoader;

        public SenderInfolistViewModel(ErgSenderLoader senderLoader)
        {
            this.senderLoader = senderLoader;

            List<IErgSender> givenSenders = this.senderLoader.GetErgSender();
            foreach (IErgSender sender in givenSenders)
            {
                SenderInfoViewModel newInfo = new SenderInfoViewModel(sender);
                Senders.Add(newInfo);
            }
        }

        public BindableCollection<SenderInfoViewModel> Senders
        {
            get
            {
                return senders;
            }
        }
    }
}
