﻿

namespace FitzLaneUI
{
    using Caliburn.Micro;
    using Events;
    using FitzLanePlugin;
    using System.Dynamic;
    class ShellViewModel : Screen, IHandle<StartEvent>, IHandle<StopEvent>
    {
        IEventAggregator eventAggregator = null;
        PlayerProviderLoader playerProviderLoader = null;
        ErgSenderLoader ergSenderLoader = null;

        public LaneContainerViewModel LaneContainer { get; }
        public LaneControlViewModel LaneControl { get; }

        public ShellViewModel()
        {
            DisplayName = "FitzLane";

            //init the EventAggregator
            eventAggregator = new EventAggregator();
            eventAggregator.Subscribe(this);

            //load the plugins
            ergSenderLoader = new ErgSenderLoader("plugins/ergsender/");
            playerProviderLoader = new PlayerProviderLoader();
            playerProviderLoader.LoadPlayerProviders("plugins/player/");

            //fire up the views
            LaneContainer = new LaneContainerViewModel(eventAggregator, playerProviderLoader, ergSenderLoader);
            LaneControl = new LaneControlViewModel(eventAggregator);
        }

        public void SaveConfig()
        {
            LaneContainer.WriteCurrentConfig();
        }

        public void ClearLanes()
        {
            LaneContainer.ClearLanes();
        }

        public void ExitApplication()
        {
            TryClose();
        }

        public void ShowPlayerInfo()
        {
            dynamic settings = new ExpandoObject();
            settings.Title = "Player Info";

            IWindowManager windowManager = new WindowManager();
            PlayerInfolistViewModel playerinfoDialog = new PlayerInfolistViewModel(playerProviderLoader);
            windowManager.ShowDialog(playerinfoDialog, null, settings);
        }

        public void ShowSenderInfo()
        {
            dynamic settings = new ExpandoObject();
            settings.Title = "Sender Info";

            IWindowManager windowManager = new WindowManager();
            SenderInfolistViewModel senderInfoDialog = new SenderInfolistViewModel(ergSenderLoader);
            windowManager.ShowDialog(senderInfoDialog, null, settings);
        }

        public void Handle(StopEvent message)
        {
            LaneContainer.StopSimulation();
        }

        public void Handle(StartEvent message)
        {
            //TODO: How to check if the sender-plugins have been configured correctly?
            LaneContainer.StartSimulation();
        }
    }
}
