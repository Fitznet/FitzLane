﻿

namespace FitzLaneUI
{
    using Caliburn.Micro;
    using Events;
    using FitzLaneConfig;
    using FitzLanePlugin.Interfaces;

    public class FullLaneViewModel : LaneViewModel
    {
        private readonly IEventAggregator eventAggregator;

        public FullLaneViewModel(IEventAggregator eventAggregator, IPlayer givenPlayer, Lane givenLane)
        {
            this.eventAggregator = eventAggregator;

            lane = givenLane;
            player = givenPlayer;
            Record = givenPlayer.IsRecordingActive;
        }

        private bool isStarted = false;
        public override bool IsStarted
        {
            get
            {
                return isStarted;
            }
            set
            {
                isStarted = value;
                NotifyOfPropertyChange(() => CanRemove);
                NotifyOfPropertyChange(() => CanEdit);
            }
        }

        public bool CanRemove
        {
            get
            {
                if (IsStarted)
                {
                    return false;
                }
                return true;
            }
        }
        public void Remove()
        {
            eventAggregator.PublishOnUIThread(new RemovePlayerEvent(player));
        }
        public bool CanEdit
        {
            get
            {
                if (IsStarted)
                {
                    return false;
                }
                return true;
            }
        }
        public void Edit()
        {
            eventAggregator.PublishOnUIThread(new EditPlayerEvent(lane));
        }

        private bool record = false;
        public bool Record
        {
            get
            {
                return record;
            }
            set
            {
                record = value;
                player.IsRecordingActive = value;
                NotifyOfPropertyChange(() => Record);
            }
        }

        IPlayer player = null;
        public IPlayer Player
        {
            get { return player; }
            set
            {
                //only update the player if the name is matching
                if(player.PlayerErg.name == value.PlayerErg.name)
                {
                    player = value;
                    NotifyOfPropertyChange(() => PlayerName);
                    NotifyOfPropertyChange(() => Distance);
                    NotifyOfPropertyChange(() => Pace);
                }
            }
        }

        private Lane lane = null;
        public Lane Lane
        {
            get { return lane; }
            set
            {
                lane = value;
                NotifyOfPropertyChange(() => PlayerType);
                NotifyOfPropertyChange(() => MainPlayer);
            }
        }

        public string PlayerName
        {
            get { return player?.PlayerErg.name; }
        }
        public string PlayerType
        {
            get { return lane?.playerType; }
        }
        public string MainPlayer
        {
            get
            {
                if (lane != null && lane.isMainPlayer)
                { return "Main Player"; }
                else
                { return ""; }
            }
        }
        public string Distance
        {
            get { return player?.PlayerErg.distance.ToString("#.00") + " m"; }
        }
        public string Pace
        {
            get { return player?.PlayerErg.paceInSecs.ToString("#.00") + " sec/500m"; }
        }
        public string Cadence
        {
            get { return player?.PlayerErg.cadence + " SPM"; }
        }
    }
}
