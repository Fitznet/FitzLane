﻿
namespace FitzLaneUI.Events
{
    class NewPlayerEvent
    {
        public NewPlayerEvent(int laneIndex)
        {
            LaneIndex = laneIndex;
        }

        public int LaneIndex { get; set; }
    }
}
