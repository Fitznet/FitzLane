﻿using FitzLaneConfig;

namespace FitzLaneUI.Events
{
    class EditPlayerEvent
    {
        public EditPlayerEvent(Lane lane) { EditLane = lane; }
        public Lane EditLane { get; set; }
    }
}
