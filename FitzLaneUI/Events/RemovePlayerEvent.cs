﻿using FitzLanePlugin.Interfaces;

namespace FitzLaneUI.Events
{
    class RemovePlayerEvent
    {
        public RemovePlayerEvent(IPlayer player)
        {
            Player = player;
        }

        public IPlayer Player { get; }
    }
}
