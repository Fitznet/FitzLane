﻿
namespace FitzLaneUI
{
    using System.Collections.Generic;
    using System.Reflection;
    using System.Windows;
    using Caliburn.Micro;
    using FitzLanePlugin;
    class FitzBootstrapper : BootstrapperBase
    {
        public FitzBootstrapper()
        {
            try
            {
                Initialize();
            }
            catch (System.Exception ex)
            {
                System.Windows.MessageBox.Show("Unhandled Exception: " + ex.ToString());
                throw ex;
            }
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            try
            {
                DisplayRootViewFor<ShellViewModel>();
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("Unhandled Exception: " + ex.ToString());
                throw ex;
            }
        }

        protected override IEnumerable<Assembly> SelectAssemblies()
        {
            try
            {
                PlayerProviderLoader playerLoader = new PlayerProviderLoader();
                IList<Assembly> assemblies = playerLoader.GetPlayerProviderAssemblies("plugins/player/");
                assemblies.Add(Assembly.GetExecutingAssembly());

                return assemblies;
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("Unhandled Exception: " + ex.ToString());
                throw ex;
            }
        }
    }
}
