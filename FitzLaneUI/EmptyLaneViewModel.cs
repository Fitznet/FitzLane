﻿
namespace FitzLaneUI
{
    using Caliburn.Micro;
    using FitzLanePlugin;
    using Events;

    public class EmptyLaneViewModel : LaneViewModel
    {
        readonly IEventAggregator eventAggregator;
        PlayerProviderLoader playerProviderLoader = null;
        bool isStarted = false;

        public EmptyLaneViewModel(IEventAggregator eventAggregator, PlayerProviderLoader playerProviderLoader, int index)
        {
            this.eventAggregator = eventAggregator;
            this.playerProviderLoader = playerProviderLoader;
            LaneIndex = index;
        }

        public override bool IsStarted
        {
            get
            {
                return isStarted;
            }
            set
            {
                isStarted = value;
                NotifyOfPropertyChange(() => CanAddPlayer);
            }
        }

        public int LaneIndex
        {
            get; set;
        }

        public bool CanAddPlayer
        {
            get
            {
                if (IsStarted)
                {
                    return false;
                }
                return true;
            }
        }

        public void AddPlayer()
        {
            eventAggregator.PublishOnUIThread(new NewPlayerEvent(LaneIndex));
        }
    }
}
