﻿
namespace FitzLaneUI
{
    using Caliburn.Micro;
    using FitzLaneConfig;
    using FitzLanePlugin;
    using FitzLanePlugin.Interfaces;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    class ConfigurePlayerViewModel : Screen
    {
        Lane lane = null;
        PlayerProviderLoader playerProviderLoader = null;
        PlayerConfigViewModel currentPlayerConfig = null;

        public ConfigurePlayerViewModel(PlayerProviderLoader playerProviderLoader, ICollection<IPlayer> currentPlayers, int givenLaneIndex)
        {
            this.playerProviderLoader = playerProviderLoader;
            ParentNames = currentPlayers.Select(p => p.PlayerErg.name).ToList();

            lane = new Lane
            {
                laneIndex = givenLaneIndex
            };

            //Fill in a first PlayerType
            SelectedPlayerType = playerProviderLoader.GetPlayerNames()[0];

            //Fill in a random name
            Random rnd = new Random();
            PlayerName = GetRandomName(rnd, currentPlayers.Select(p => p.PlayerErg.name));
        }


        public ConfigurePlayerViewModel(PlayerProviderLoader playerProviderLoader, ICollection<IPlayer> currentPlayers, Lane givenLane)
        {
            //TODO: How to unset a parent?

            this.playerProviderLoader = playerProviderLoader;
            ParentNames = currentPlayers.Select(p => p.PlayerErg.name).Where(name => !name.Equals(givenLane.name)).ToList();

            lane = givenLane;
            PlayerName = lane.name;
            SelectedParentName = lane.parentName;
            IsMainPlayer = lane.isMainPlayer;
            SelectedPlayerType = lane.playerType;
            PlayerConfig.SetPlayerConfig(lane.playerConfig);
        }

        public string GetRandomName(Random rnd, IEnumerable<string> currentPlayerNames)
        {
            //These are the names of current male world record holders on C2 ergometers
            //Source: http://www.concept2.com/indoor-rowers/racing/records/world/2000
            List<string> playerNames = new List<string>
            {
                "Andy Ripley",
                "Arnold Cooke",
                "Brian Bailey",
                "Charles Hamlin",
                "Dean Smith",
                "Eskild Ebbesen",
                "Geoffrey Knight",
                "Graham Benton",
                "Henrik Stephansen",
                "Jesús González",
                "John Hodgson",
                "Karsten Brodowsky",
                "Matt Phillips",
                "Michael Johnson",
                "Paul Guest",
                "Paul Hendershott",
                "Paul Siebach",
                "Philipp-Andre Syring",
                "Rob Waddell",
                "Robert Spenger",
                "Roger Bangay",
                "Roger Borggaard",
                "Roy Brook",
                "Stephen Richardson",
                "Thomas Darling",
                "Tor Arne Simonsen"
            };

            //Remove names that already exist
            playerNames = playerNames.Except(currentPlayerNames).ToList();

            int rndIndex = rnd.Next(playerNames.Count);
            return playerNames[rndIndex];
        }

        public Lane ConfiguredLane
        {
            get
            {
                lane.playerConfig = currentPlayerConfig.GetPlayerConfig();
                return lane;
            }
        }

        public string PlayerName
        {
            get
            {
                return lane.name;
            }
            set
            {
                lane.name = value;
            }
        }

        public bool IsMainPlayer
        {
            get
            {
                return lane.isMainPlayer;
            }
            set
            {
                lane.isMainPlayer = value;
            }
        }

        public List<string> ParentNames { get; }
        public string SelectedParentName
        {
            get
            {
                return lane.parentName;
            }
            set
            {
                lane.parentName = value;
            }
        }

        public List<string> PlayerTypes
        {
            get
            {
                return playerProviderLoader.GetPlayerNames();
            }
        }

        public string SelectedPlayerType
        {
            get
            {
                return lane.playerType;
            }
            set
            {
                lane.playerType = value;
                NotifyOfPropertyChange(() => PlayerConfig);
            }
        }

        public PlayerConfigViewModel PlayerConfig
        {
            get
            {
                var playerProviders = playerProviderLoader.GetPlayerProvider();
                foreach (IPlayerProvider provider in playerProviders)
                {
                    if(provider.IsValidPlayertype(SelectedPlayerType))
                    {
                        currentPlayerConfig = provider.GetPlayerConfigViewModel();
                        if(lane.playerConfig != null)
                        {
                            currentPlayerConfig.SetPlayerConfig(lane.playerConfig);
                        }
                        return currentPlayerConfig;
                    }
                }

                return new PlayerConfigViewModel();
            }
        }

        public void Done()
        {
            TryClose();
        }
    }
}
