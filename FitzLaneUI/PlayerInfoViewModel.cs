﻿
namespace FitzLaneUI
{
    using FitzLanePlugin.Interfaces;

    class PlayerInfoViewModel
    {
        public PlayerInfoViewModel(IPlayerProvider provider)
        {
            PlayerName = provider.PlayerName;
            PlayerDescription = provider.PlayerDescription;
            PlayerVersion = "Version: " + provider.PlayerVersion;
        }

        public string PlayerName
        {
            get; set;
        }
        public string PlayerDescription
        {
            get; set;
        }
        public string PlayerVersion
        {
            get; set;
        }
    }
}
