﻿
namespace C2Connector
{
    using FitzLanePlugin;
    using System.IO;
    using System.Runtime.Serialization.Json;
    using System.Text;

    class C2ConnectorConfigViewModel : PlayerConfigViewModel
    {
        public C2ConnectorConfigViewModel()
        {
            ErgAddress = 0;
        }

        public override string GetPlayerConfig()
        {
            C2ConnectorConfig cfg = new C2ConnectorConfig
            {
                ErgAddress = ErgAddress
            };
            MemoryStream memStream = new MemoryStream();
            DataContractJsonSerializer botSerializer = new DataContractJsonSerializer(typeof(C2ConnectorConfig));
            botSerializer.WriteObject(memStream, cfg);
            return Encoding.UTF8.GetString(memStream.ToArray());
        }

        public override void SetPlayerConfig(string config)
        {
            //read the config
            MemoryStream memStream = new MemoryStream();
            StreamWriter strWriter = new StreamWriter(memStream);
            strWriter.Write(config);
            strWriter.Flush();
            memStream.Position = 0;
            DataContractJsonSerializer playerSerializer = new DataContractJsonSerializer(typeof(C2ConnectorConfig));
            C2ConnectorConfig cfg = (C2ConnectorConfig)playerSerializer.ReadObject(memStream);

            ErgAddress = cfg.ErgAddress;
        }
        public uint ErgAddress
        {
            get; set;
        }
    }
}
