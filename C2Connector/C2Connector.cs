﻿
using Concept2;
using EasyErgsocket;
using FitzLanePlugin.Interfaces;

using System.IO;
using System.Runtime.Serialization.Json;

namespace C2Connector
{
    class C2Connector : IPlayer
    {
        PerformanceMonitor pm;

        public bool IsRecordingActive { get; set; } = true;
        public Erg PlayerErg { get; set; }

        public C2Connector(string name, string config)
        {
            //read the config
            MemoryStream memStream = new MemoryStream();
            StreamWriter strWriter = new StreamWriter(memStream);
            strWriter.Write(config);
            strWriter.Flush();
            memStream.Position = 0;
            DataContractJsonSerializer playerSerializer = new DataContractJsonSerializer(typeof(C2ConnectorConfig));
            C2ConnectorConfig cfg = (C2ConnectorConfig)playerSerializer.ReadObject(memStream);

            //init the C2 interface
            PMUSBInterface.Initialize();
            PMUSBInterface.InitializeProtocol(999);

            ushort numErgs = PMUSBInterface.DiscoverPMs(PMUSBInterface.PMtype.PM3TESTER_PRODUCT_NAME);
            numErgs += PMUSBInterface.DiscoverPMs(PMUSBInterface.PMtype.PM3_PRODUCT_NAME);
            numErgs += PMUSBInterface.DiscoverPMs(PMUSBInterface.PMtype.PM3_PRODUCT_NAME2);
            numErgs += PMUSBInterface.DiscoverPMs(PMUSBInterface.PMtype.PM4_PRODUCT_NAME);
            numErgs += PMUSBInterface.DiscoverPMs(PMUSBInterface.PMtype.PM5_PRODUCT_NAME);
            if(numErgs == 0)
            {
                //TODO: This means that no erg has been found... what to do?
            }

            pm = new PerformanceMonitor((ushort)cfg.ErgAddress)
            {
                //init everything, this avoids us crashing if no connection is made to the erg
                Calories = 0,
                DeviceNumber = 0,
                Distance = 0.0f,
                DragFactor = 0,
                Heartrate = 0,
                Pace = 0.0f,
                Power = 0,
                Serial = "",
                SPM = 0,
                SPMAvg = 0.0f,
                StrokePhase = StrokePhase.Idle,
                Worktime = 0.0f
            };

            try
            {
                pm.StatusUpdate();
            }
            catch (PMUSBInterface.PMUSBException)
            {
                //No Erg found, USB exception...
            }


            PlayerErg = new Erg
            {
                cadence = 0,
                calories = 0,
                distance = 0.0,
                ergtype = ErgType.ROW,
                exerciseTime = 0.0,
                heartrate = 0,
                name = name,
                paceInSecs = 0,
                playertype = PlayerType.HUMAN,
                power = 0,
                ergId = pm.Serial
            };
        }

        public string ParentName
        {
            get; set;
        }

        public void Reset()
        {}

        public void Update(Erg givenParent = null)
        {
            //NOTE: This erg ignores parent ergometers...
            try
            {
                pm.LowResolutionUpdate();
            }
            catch (PMUSBInterface.PMUSBException)
            {
                //No Erg found, USB exception...
            }

            PlayerErg.cadence = pm.SPM;
            PlayerErg.calories = pm.Calories;
            PlayerErg.distance = pm.Distance;
            PlayerErg.exerciseTime = pm.Worktime;
            PlayerErg.heartrate = pm.Heartrate;
            PlayerErg.paceInSecs = (uint)pm.Pace;
            PlayerErg.power = pm.Power;
        }
    }
}
