﻿using System.Runtime.Serialization;

namespace C2Connector
{
    [DataContract]
    class C2ConnectorConfig
    {
        [DataMember]
        public uint ErgAddress = 0;
    }
}
