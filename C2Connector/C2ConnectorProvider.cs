﻿
using System;
using FitzLanePlugin;
using FitzLanePlugin.Interfaces;

namespace C2Connector
{
    class C2ConnectorProvider : IPlayerProvider
    {
        public string GetDefaultPlayerConfig()
        {
            //NOTE: This method won't be used in the future...
            return "";
        }

        public IPlayer GetPlayer(string name, string config)
        {
            return new C2Connector(name, config);
        }

        public object GetPlayerConfigView()
        {
            //NOTE: This method won't be used in the future
            return new object();
        }

        public PlayerConfigViewModel GetPlayerConfigViewModel()
        {
            return new C2ConnectorConfigViewModel();
        }

        public bool IsValidPlayertype(string playerType)
        {
            if (playerType == PlayerName)
            {
                return true;
            }
            return false;
        }
        
        public string PlayerDescription
        {
            get { return "Connects to a Concept2 Rowing Ergometer (PM3, PM4, PM5)"; }
        }
        public string PlayerName
        {
            get { return "C2Connector"; }
        }
        public string PlayerVersion
        {
            get { return "1.0"; }
        }
    }
}
