﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace FitzLaneConfig
{
    [DataContract]
    public class Lane
    {
        /// <summary>
        ///     Unique index of lane this configuration is for.
        /// </summary>
        [DataMember]
        public int laneIndex = 0;

        /// <summary>
        ///     This flag defines if the player shall be focused by the camera.
        /// </summary>
        [DataMember]
        public bool isMainPlayer = false;

        /// <summary>
        ///     Player name used for display.
        /// </summary>
        [DataMember]
        public string name = "";

        /// <summary>
        ///     Defines a dependency to another Lane.
        /// </summary>
        [DataMember]
        public string parentName = "";

        /// <summary>
        ///     Type of player, i.e., what kind of instance should be created.
        /// </summary>
        [DataMember]
        public string playerType = "";

        /// <summary>
        ///     Configuration specific to the used playerType.
        /// </summary>
        [DataMember]
        public string playerConfig;
    }



    [DataContract]
    public class LanesContainer
    {
        [DataMember]
        public List<Lane> laneList = new List<Lane>();
    }
}
