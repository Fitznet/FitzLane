﻿using System;
using System.IO;
using System.Runtime.Serialization.Json;
using EasyErgsocket;
using FitzLanePlugin.Interfaces;

namespace BotConstantPlugin
{
    class BotConstant : IPlayer
    {
        private readonly uint originalPace = 0;   //do not forget the original pace in case the bot gets reset
        private readonly double amplitude = 0.1;  //amplitude with which the rowing movement is calculated
        private double offsetTime = 0.0; //needed if the boat changes its pace
        private double offsetDist = 0.0; //needed if the boat changes its pace

        //this is needed to measure the time if this bot is not attached to any parent lane
        private DateTime starttime = DateTime.MinValue;

        public string ParentName { get; set; } = "";
        public bool IsRecordingActive { get; set; } = false;
        public Erg PlayerErg { get; set; }

        public BotConstant(string name, string config)
        {
            //read the config
            MemoryStream memStream = new MemoryStream();
            StreamWriter strWriter = new StreamWriter(memStream);
            strWriter.Write(config);
            strWriter.Flush();
            memStream.Position = 0;
            DataContractJsonSerializer playerSerializer = new DataContractJsonSerializer(typeof(BotConstantConfig));
            BotConstantConfig botCfg = (BotConstantConfig)playerSerializer.ReadObject(memStream);

            PlayerErg = new Erg
            {
                cadence = botCfg.spm,
                calories = 1,
                distance = 0.0,
                ergtype = ErgType.ROW,
                exerciseTime = 10.0,
                heartrate = 0,
                name = name,
                paceInSecs = botCfg.pace,
                playertype = PlayerType.BOT,
                power = 0,
                ergId = name
            };

            originalPace = PlayerErg.paceInSecs;
        }

        public void Reset()
        {
            PlayerErg.paceInSecs = originalPace;
            offsetTime = 0.0;
            offsetDist = 0.0;
            starttime = DateTime.MinValue;
        }

        public void Update(Erg givenParent = null)
        {
            if (starttime == DateTime.MinValue)
            {
                starttime = DateTime.Now;
            }

            if (givenParent == null)
            {
                //if there is no parent to give the time use the time that has passed since starting the application
                TimeSpan timeSinceStart = DateTime.Now - starttime;
                PlayerErg.exerciseTime = timeSinceStart.TotalSeconds;
            }
            else
            {
                PlayerErg.exerciseTime = givenParent.exerciseTime;
            }
            double strokesPerSecond = PlayerErg.cadence / 60.0;
            double velocity = 500.0 / PlayerErg.paceInSecs;

            double timePassedOffset = (PlayerErg.exerciseTime - offsetTime); //apply the offset onto the time
            double timeCalc = timePassedOffset + amplitude * -Math.Sin(timePassedOffset * strokesPerSecond * 2.0 * Math.PI);
            PlayerErg.distance = offsetDist + (velocity * timeCalc);
        }

        public void ChangePace(uint newPace)
        {
            PlayerErg.paceInSecs = newPace;
            offsetTime = PlayerErg.exerciseTime;
            offsetDist = PlayerErg.distance;
        }
    }
}
