﻿
namespace BotConstantPlugin
{
    using FitzLanePlugin;
    using System.IO;
    using System.Runtime.Serialization.Json;
    using System.Text;

    public class BotConstantConfigViewModel : PlayerConfigViewModel
    {
        public BotConstantConfigViewModel()
        {
            Pace = 120;
            Spm = 20;
        }

        public override string GetPlayerConfig()
        {
            BotConstantConfig botCfg = new BotConstantConfig
            {
                pace = Pace,
                spm = Spm
            };
            MemoryStream memStream = new MemoryStream();
            DataContractJsonSerializer botSerializer = new DataContractJsonSerializer(typeof(BotConstantConfig));
            botSerializer.WriteObject(memStream, botCfg);
            return Encoding.UTF8.GetString(memStream.ToArray());
        }

        public override void SetPlayerConfig(string config)
        {
            //read the config
            MemoryStream memStream = new MemoryStream();
            StreamWriter strWriter = new StreamWriter(memStream);
            strWriter.Write(config);
            strWriter.Flush();
            memStream.Position = 0;
            DataContractJsonSerializer playerSerializer = new DataContractJsonSerializer(typeof(BotConstantConfig));
            BotConstantConfig botCfg = (BotConstantConfig)playerSerializer.ReadObject(memStream);
            Pace = botCfg.pace;
            Spm = botCfg.spm;
        }

        public uint Pace
        {
            get; set;
        }

        public uint Spm
        {
            get; set;
        }
    }
}
