﻿
using FitzLanePlugin.Interfaces;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
using FitzLanePlugin;
using System;

namespace BotConstantPlugin
{
    class BotConstantProvider : IPlayerProvider
    {
        public IPlayer GetPlayer(string name, string config)
        {
            return new BotConstant(name, config);
        }

        public string GetDefaultPlayerConfig()
        {
            BotConstantConfig botCfg = new BotConstantConfig();
            MemoryStream memStream = new MemoryStream();
            DataContractJsonSerializer botSerializer = new DataContractJsonSerializer(typeof(BotConstantConfig));
            botSerializer.WriteObject(memStream, botCfg);
            return Encoding.UTF8.GetString(memStream.ToArray());
        }

        public bool IsValidPlayertype(string playerType)
        {
            if (playerType == "BotConstant")
            {
                return true;
            }
            return false;
        }

        public PlayerConfigViewModel GetPlayerConfigViewModel()
        {
            return new BotConstantConfigViewModel();
        }

        public string PlayerDescription
        {
            get { return "This Bot rows with a constant pace while showing a typical rowing movement"; }
        }
        public string PlayerName
        {
            get { return "BotConstant"; }
        }
        public string PlayerVersion
        {
            get { return "1.0.0"; }
        }
    }
}
