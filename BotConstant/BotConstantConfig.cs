﻿using System.Runtime.Serialization;

namespace BotConstantPlugin
{
    [DataContract]
    class BotConstantConfig
    {
        [DataMember]
        public uint pace = 120;

        [DataMember]
        public uint spm = 20;
    }
}